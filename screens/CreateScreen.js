import React, { useState } from "react";
import { StyleSheet, Text, View, ScrollView } from "react-native";
import {
  CardItem,
  Body,
  Card,
  Row,
  Col,
  Textarea,
  Button,
  Item,
  Input,
  DatePicker,
  Icon,
} from "native-base";

const CreateScreen = () => {
  const [dueDate, setdueDate] = useState(new Date());
  const [issueDate, setissueDate] = useState(new Date());

  return (
    <ScrollView style={styles.container}>
      <View>
        <Row style={styles.row}>
          <Col>
            <Text style={styles.headerText}>Company Information</Text>
            <Card style={styles.card}>
              <CardItem>
                <Body>
                  <View style={{ width: "100%" }}>
                    <Text style={styles.textInput}>Logo</Text>
                    <Text style={styles.textInput}>//File picker here</Text>
                  </View>
                  <View style={{ width: "100%" }}>
                    <Text style={styles.textInput}>Name</Text>
                    <Item regular style={styles.itemInput}>
                      <Input placeholder="" />
                    </Item>
                  </View>
                  <View style={{ width: "100%" }}>
                    <Text style={styles.textInput}>Tax Id</Text>
                    <Item regular style={styles.itemInput}>
                      <Input placeholder="" />
                    </Item>
                  </View>
                  <View style={{ width: "100%" }}>
                    <Text style={styles.textInput}>E-mail Address</Text>
                    <Item regular style={styles.itemInput}>
                      <Input placeholder="" />
                    </Item>
                  </View>
                  <View style={{ width: "100%" }}>
                    <Text style={styles.textInput}>Phone Number</Text>
                    <Item regular style={styles.itemInput}>
                      <Input placeholder="" />
                    </Item>
                  </View>
                  <View style={{ width: "100%" }}>
                    <Text style={styles.textInput}>Address</Text>
                    <Textarea
                      rowSpan={4}
                      bordered
                      placeholder=""
                      style={styles.textArea}
                    />
                  </View>
                </Body>
              </CardItem>
            </Card>
          </Col>
        </Row>

        <Row style={styles.row}>
          <Col>
            <Text style={styles.headerText}>Your Customer</Text>
            <Card style={styles.card}>
              <CardItem>
                <Body>
                  <View style={{ width: "100%" }}>
                    <Text style={styles.textInput}>Name</Text>
                    <Item regular style={styles.itemInput}>
                      <Input placeholder="" />
                    </Item>
                  </View>
                  <View style={{ width: "100%" }}>
                    <Text style={styles.textInput}>Tax Id</Text>
                    <Item regular style={styles.itemInput}>
                      <Input placeholder="" />
                    </Item>
                  </View>
                  <View style={{ width: "100%" }}>
                    <Text style={styles.textInput}>E-mail Address</Text>
                    <Item regular style={styles.itemInput}>
                      <Input placeholder="" />
                    </Item>
                  </View>
                  <View style={{ width: "100%" }}>
                    <Text style={styles.textInput}>Phone Number</Text>
                    <Item regular style={styles.itemInput}>
                      <Input placeholder="" />
                    </Item>
                  </View>
                  <View style={{ width: "100%" }}>
                    <Text style={styles.textInput}>Address</Text>
                    <Textarea
                      rowSpan={4}
                      bordered
                      placeholder=""
                      style={styles.textArea}
                    />
                  </View>
                </Body>
              </CardItem>
            </Card>
          </Col>
        </Row>

        <Row style={styles.row}>
          <Col>
            <Text style={styles.headerText}>Invoice Information</Text>
            <Card style={styles.card}>
              <CardItem>
                <Body>
                  <View style={{ width: "100%" }}>
                    <Text style={styles.textInput}>No.</Text>
                    <Item regular style={styles.itemInput}>
                      <Input placeholder="" />
                    </Item>
                  </View>
                  <View style={{ width: "100%" }}>
                    <Text style={styles.textInput}>Issue Date</Text>
                    <Item regular style={styles.itemInput}>
                      <Input
                        placeholder=""
                        disabled
                        value={issueDate.toString().substr(4, 12)}
                        style={styles.dataInput}
                      />
                      <Icon
                        type="FontAwesome5"
                        name="calendar-alt"
                        style={styles.dateIcon}
                      >
                        <DatePicker
                          defaultDate={issueDate}
                          locale={"th"}
                          timeZoneOffsetInMinutes={undefined}
                          modalTransparent={false}
                          animationType={"slide"}
                          androidMode={"default"}
                          textStyle={{}}
                          placeHolderTextStyle={{ color: "#fff" }}
                          onDateChange={(d) => {
                            setissueDate(d);
                          }}
                          disabled={false}
                        />
                      </Icon>
                    </Item>
                  </View>
                  <View style={{ width: "100%" }}>
                    <Text style={styles.textInput}>Due Date</Text>
                    <Item regular style={styles.itemInput}>
                      <Input
                        placeholder=""
                        disabled
                        value={dueDate.toString().substr(4, 12)}
                        style={styles.dataInput}
                      />
                      <Icon
                        type="FontAwesome5"
                        name="calendar-alt"
                        style={styles.dateIcon}
                      >
                        <DatePicker
                          defaultDate={dueDate}
                          locale={"th"}
                          timeZoneOffsetInMinutes={undefined}
                          modalTransparent={false}
                          animationType={"slide"}
                          androidMode={"default"}
                          textStyle={{}}
                          placeHolderTextStyle={{ color: "#fff" }}
                          onDateChange={(d) => {
                            setdueDate(d);
                          }}
                          disabled={false}
                        />
                      </Icon>
                    </Item>
                  </View>
                </Body>
              </CardItem>
            </Card>
          </Col>
        </Row>

        <Row style={styles.row}>
          <Col>
            <Text style={styles.headerText}>Items</Text>
            <Card>
              <CardItem>
                <Body>
                  <Text>//Your text here</Text>
                </Body>
              </CardItem>
            </Card>
          </Col>
        </Row>

        <Row style={styles.row}>
          <Col>
            <Text style={styles.headerText}>Note</Text>
            <Card style={styles.card}>
              <CardItem>
                <Body>
                  <Textarea
                    rowSpan={8}
                    bordered
                    placeholder=""
                    style={styles.textArea}
                  />
                </Body>
              </CardItem>
            </Card>
          </Col>
        </Row>
        <Row style={styles.row}>
          <Button block style={styles.button}>
            <Text style={styles.buttonText}>CREATE</Text>
          </Button>
        </Row>
      </View>
    </ScrollView>
  );
};

export default CreateScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F8F8F7",
    padding: 16,
  },
  row: {
    marginBottom: 16,
  },
  headerText: {
    fontSize: 16,
    fontFamily: "Kanit_semi",
  },
  card: {
    padding: 16,
    borderRadius: 8,
  },
  textArea: {
    width: "100%",
    borderRadius: 8,
  },
  button: {
    width: "100%",
    height: 48,
    borderRadius: 24,
    backgroundColor: "#74957C",
    marginVertical: 44,
  },
  buttonText: {
    textAlign: "center",
    fontFamily: "Kanit_semi",
    fontSize: 16,
    color: "#fff",
  },
  itemInput: { height: 32, marginTop: 4, borderRadius: 8, marginBottom: 8 },
  textInput: {
    color: "#aaa",
    fontSize: 14,
    fontFamily: "Kanit",
  },
  dateIcon: {
    position: "absolute",
    right: -12,
    top: 0,
    fontSize: 14,
    color: "#CCCCCC",
  },
  dataInput: {
    color: "#000",
    fontFamily: "Kanit",
    fontSize: 14,
  },
});
