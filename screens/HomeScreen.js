import * as WebBrowser from "expo-web-browser";
import * as React from "react";
import {
  Image,
  Platform,
  StyleSheet,
  TouchableOpacity,
  View,
  FlatList,
} from "react-native";
import { ScrollView } from "react-native-gesture-handler";

import { MonoText } from "../components/StyledText";
import { HeaderTitle } from "@react-navigation/stack";
import Header from "../components/Header";
import { getStatusBarHeight } from "react-native-status-bar-height";
import { Button, Text, Row, Col } from "native-base";
import { FontAwesome } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";

const data = [
  {
    id: "ID-2-27",
    name: "Invoice 1",
    customer: "A company",
    date: "12-April-2020",
    due: "1-2-3",
  },
  {
    id: "ID-2-28",
    name: "Invoice 2",
    customer: "A company",
    date: "9-July-2020",
    due: "1-2-3",
  },
  {
    id: "ID-2-29",
    name: "Invoice 3",
    customer: "B company",
    date: "23-July-2020",
    due: "1-2-3",
  },
];

function Item({ data }) {
  return (
    <View style={styles.item}>
      <Row>
        <View style={styles.itemLogo}></View>
        <Col>
          <Text style={styles.title}>{data.customer}</Text>
          <Text style={styles.date}>{data.date}</Text>
        </Col>
      </Row>
    </View>
  );
}

export default function HomeScreen() {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <Header />
      <Button
        rounded
        style={styles.button}
        onPress={() => navigation.navigate("Create")}
      >
        <FontAwesome name="plus" size={12} color="white" />
        <Text style={styles.buttonText}>NEW</Text>
      </Button>
      <View style={styles.listView}>
        {/* Flastlist here */}
        <FlatList
          data={data}
          renderItem={({ item }) => <Item data={item} />}
          keyExtractor={(item) => item.id}
        />
      </View>
    </View>
  );
}

HomeScreen.navigationOptions = {
  header: null,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    marginTop: getStatusBarHeight(),
  },
  button: {
    width: 96,
    height: 48,
    position: "absolute",
    right: 16,
    top: 45,
    paddingHorizontal: 21,
    backgroundColor: "#74957C",
  },
  buttonText: {
    fontSize: 16,
    fontWeight: "bold",
    marginLeft: -10,
  },
  listView: {
    marginTop: 24,
  },
  item: {
    backgroundColor: "#F8F8F7",
    marginHorizontal: 16,
    marginBottom: 12,
    height: 72,
    borderRadius: 12,
  },
  title: {
    marginTop: 16,
    color: "#666666",
  },
  itemLogo: {
    width: 40,
    height: 40,
    marginVertical: 16,
    marginLeft: 24,
    marginRight: 16,
    backgroundColor: "#9DC6A7",
    borderRadius: 8,
  },
  date: {
    color: "#999999",
  },
});
