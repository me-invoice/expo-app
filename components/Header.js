import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Row } from "native-base";

const Header = () => {
  return (
    <View style={styles.container}>
      <Row style={styles.row}>
        <Text style={styles.me}>Me</Text>
        <Text style={styles.invoice}>Invoice</Text>
      </Row>
      <Row style={styles.row}>
        <Text style={styles.all}>ALL INVOICES</Text>
      </Row>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: 120,
    marginTop: 8,
    // backgroundColor: "#aaa",
  },
  me: {
    fontSize: 50,
    fontWeight: "bold",
    fontFamily: "Dosis_semi",
    color: "#707070",
  },
  invoice: {
    fontSize: 50,
    fontWeight: "bold",
    fontFamily: "Dosis_semi",
    color: "#9DC6A7",
  },
  row: {
    marginLeft: 22,
  },
  all: {
    fontSize: 20,
    fontWeight: "bold",
    fontFamily: "Kanit",
    color: "#9DC6A7",
  },
});
