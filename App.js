import React, { useState, useEffect } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { Platform, StatusBar, StyleSheet, View, Image } from "react-native";
import * as Font from "expo-font";
import { Ionicons } from "@expo/vector-icons";
import { AppLoading, SplashScreen } from "expo";
import { Asset } from "expo-asset";
import HomeScreen from "./screens/HomeScreen";
import CreateScreen from "./screens/CreateScreen";
import { Button } from "native-base";

const Stack = createStackNavigator();

export default function App(props) {
  // const isLoadingComplete = useCachedResources();
  const [isSplashReady, setIsSplashReady] = useState(false);
  const [isAppReady, setIsAppReady] = useState(false);

  const _cacheSplashResourcesAsync = async () => {
    const gif = require("./assets/images/splash.png");
    return Asset.fromModule(gif).downloadAsync();
  };

  const _cacheResourcesAsync = async () => {
    SplashScreen.hide();
    await Promise.all([
      Asset.loadAsync([
        //img here
        require("./assets/images/splash.png"),
      ]),

      Font.loadAsync({
        Roboto: require("native-base/Fonts/Roboto.ttf"),
        Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
        Dosis: require("./assets/fonts/Dosis-VariableFont_wght.ttf"),
        Dosis_semi: require("./assets/fonts/Dosis-SemiBold.ttf"),
        Kanit: require("./assets/fonts/Kanit-Regular.ttf"),
        Kanit_semi: require("./assets/fonts/Kanit-SemiBold.ttf"),

        ...Ionicons.font,
      }),
    ]);
    setTimeout(() => setIsAppReady(true), 5000);
  };

  if (!isSplashReady) {
    return (
      <AppLoading
        startAsync={_cacheSplashResourcesAsync}
        autoHideSplash={false}
        onFinish={() => setIsSplashReady(true)}
        onError={handleLoadingError}
      />
    );
  }

  if (!isAppReady) {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: "pink",
        }}
      >
        <Image
          source={require("./assets/images/splash.png")}
          resizeMode="contain"
          style={{ width: 250, height: 250 }}
          onLoad={_cacheResourcesAsync}
        />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      {Platform.OS === "ios" && (
        <StatusBar backgroundColor="#fff" translucent barStyle="dark-content" />
      )}
      {Platform.OS === "android" && <StatusBar barStyle="dark-content" />}
      {/* <Provider store={store}> */}
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            options={{
              header: () => {
                null;
              },
            }}
            name="Home"
            component={HomeScreen}
          />
          <Stack.Screen
            name="Create"
            options={{
              title: "Create Invoice",
              headerStyle: {
                backgroundColor: "#F8F8F7",
              },
              headerTitleStyle: {
                fontFamily: "Kanit",
                fontSize: 16,
              },
              headerLeft: () => (
                <Button
                  onPress={() => alert("This is a button!")}
                  title="Info"
                  color="#fff"
                />
              ),
            }}
            component={CreateScreen}
          />
        </Stack.Navigator>
      </NavigationContainer>
      {/* <HomeScreen /> */}
      {/* </Provider> */}
    </View>
  );
}

function handleLoadingError(error) {
  // In this case, you might want to report the error to your error reporting
  // service, for example Sentry
  console.warn(error);
}

function handleFinishLoading(setLoadingComplete) {
  setLoadingComplete(true);
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F8F8F7",
  },
});
